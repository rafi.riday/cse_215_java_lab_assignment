package main.course;

import java.util.ArrayList;

import main.person.Student;

public class FacultyCourse extends Course {
    private ArrayList<Student> students = new ArrayList<Student>();

    public FacultyCourse(String nameOfCourse, String courseCode, int credit) {
        super(nameOfCourse, courseCode, credit);
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void addStudent(Student s) {
        try {
            boolean found = false;

            for (StudentCourse c : s.getCourses())
                if (c.getCourseCode() == super.getCourseCode()) {
                    found = true;
                    break;
                }

            if (found == false)
                s.addCourse(new StudentCourse(super.getNameOfCourse(), super.getCourseCode(), super.getCredit()));

            for (Student student : students)
                if (student.getId() == s.getId()) {
                    System.out.println("Student is already present, no need to add\n");
                    return;
                }

            students.add(s);
            System.out.println("Student added successfully\n");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void gradeStudent(String id, double grade) {
        for (Student s : students)
            if (s.getId() == id)
                for (StudentCourse c : s.getCourses())
                    if (c.getCourseCode() == super.getCourseCode()) {
                        c.setGrade(grade);
                        System.out.println("Graded successfully\n");
                        return;
                    }

        System.out.println("Student not found\n");
    }

    public void publishGrade(String id) {
        for (Student s : students)
            if (s.getId() == id)
                for (StudentCourse c : s.getCourses())
                    if (c.getCourseCode() == super.getCourseCode()) {
                        c.setGradePublished(true);
                        System.out.println("Grade published successfully\n");
                        return;
                    }

        System.out.println("Student not found\n");
    }

    public void publishGradeAll() {
        for (Student s : students)
            for (StudentCourse c : s.getCourses())
                if (c.getCourseCode() == super.getCourseCode())
                    c.setGradePublished(true);

        System.out.println("Grade published successfully for all students\n");
    }

    @Override
    public String toString() {
        return super.toString()
                + "  Students : " + students.size()
                + '\n';
    }
}
