package main.course;

public class StudentCourse extends Course {
    private double grade;
    private boolean gradePublished;

    public StudentCourse(String nameOfCourse, String courseCode, int credit) {
        super(nameOfCourse, courseCode, credit);
        gradePublished = false;
    }

    public double getGrade() throws Exception {
        if (gradePublished)
            return grade;
        else
            throw new Exception("Grades of some courses are unpublished\n");
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public boolean isGradePublished() {
        return gradePublished;
    }

    public void setGradePublished(boolean gradePublished) {
        this.gradePublished = gradePublished;
    }

    @Override
    public String toString() {
        return super.toString()
                + "  Grade\t: " + (isGradePublished() ? grade : "not published")
                + '\n';
    }
}
