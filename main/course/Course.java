package main.course;

public class Course {
    private String nameOfCourse, courseCode;
    private int credit;

    public Course(String nameOfCourse, String courseCode, int credit) {
        this.nameOfCourse = nameOfCourse;
        this.courseCode = courseCode;
        this.credit = credit;
    }

    public String getNameOfCourse() {
        return nameOfCourse;
    }

    public void setNameOfCourse(String nameOfCourse) {
        this.nameOfCourse = nameOfCourse;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    @Override
    public String toString() {
        return "  Name\t: " + getNameOfCourse()
                + "\n  Code\t: " + getCourseCode()
                + "\n  Credit: " + getCredit()
                + '\n';
    }
}
