package main.person;

public class Person {
    private String name;
    private int age;
    private String address;

    public Person() {
        this.name = null;
        this.address = null;
        this.age = 0;
    }

    public Person(String name, int age, String address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Name\t: " + getName()
                + "\nAge\t: " + getAge()
                + "\nAddress\t: " + getAddress()
                + '\n';
    }
}
