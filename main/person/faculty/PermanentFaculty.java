package main.person.faculty;

import main.course.FacultyCourse;

public class PermanentFaculty extends Faculty {
    private double basicSalary;
    private FacultyCourse[] courses = new FacultyCourse[7];
    private int numberOfCourses;

    public PermanentFaculty(String name, int age, String address) {
        super(name, age, address);
        basicSalary = 5000;
    }

    @Override
    public double getSalary() {
        return basicSalary + basicSalary * .2 * numberOfCourses;
    }

    @Override
    public void addCourse(FacultyCourse course) throws Exception {
        if (numberOfCourses < courses.length)
            courses[numberOfCourses++] = course;
        else
            throw new Exception("Maximum number of courses reached\n");
    }

    @Override
    public FacultyCourse getCourse(String courseCode) throws Exception {
        for (FacultyCourse c : courses)
            if (c.getCourseCode() == courseCode)
                return c;

        throw new Exception("Course not found\n");
    }

    @Override
    public void printCourses() {
        System.out.println("Course details :");
        for (int i = 0; i < numberOfCourses; i++)
            System.out.println(courses[i].toString());
    }

    @Override
    public int getNumberOfCourses() {
        return numberOfCourses;
    }

    @Override
    public String toString() {
        return super.toString()
                + "Number of Courses : " + getNumberOfCourses()
                + "\nSalary\t: " + getSalary()
                + '\n';
    }
}
