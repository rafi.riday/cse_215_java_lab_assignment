package main.person.faculty;

import main.course.FacultyCourse;
import main.person.Person;

public abstract class Faculty extends Person {
    public Faculty(String name, int age, String address) {
        super(name, age, address);
    }

    public abstract double getSalary();

    public abstract void addCourse(FacultyCourse course) throws Exception;

    public abstract FacultyCourse getCourse(String courseCode) throws Exception;

    public abstract void printCourses();

    public abstract int getNumberOfCourses();

    @Override
    public String toString() {
        return super.toString();
    }
}
