package main.person;

import main.course.StudentCourse;

public class Student extends Person {
    private double cgpa;
    private int creditCompleted;
    private String id;
    private StudentCourse[] courses;
    private int numberOfCourses;

    public Student() {
        this.cgpa = 0;
        creditCompleted = 0;
        this.id = null;
        courses = new StudentCourse[0];

        numberOfCourses = 0;
    }

    public Student(double cgpa, String id, int creditCompleted) {
        this.cgpa = cgpa;
        this.creditCompleted = creditCompleted;
        this.id = id;

        setCoursesLimit(cgpa);
        numberOfCourses = 0;
    }

    public Student(String name, int age, String address, double cgpa, String id, int creditCompleted) {
        super(name, age, address);
        this.cgpa = cgpa;
        this.creditCompleted = creditCompleted;
        this.id = id;

        setCoursesLimit(cgpa);
        numberOfCourses = 0;
    }

    public StudentCourse[] getCourses() {
        return courses;
    }

    public void setCoursesLimit(double cgpa) {
        if (cgpa < 2.6)
            courses = new StudentCourse[4];
        else
            courses = new StudentCourse[6];
    }

    public double getCgpa() {
        return cgpa;
    }

    public void setCgpa(double cgpa) {
        this.cgpa = cgpa;
        courses = null;
        setCoursesLimit(cgpa);
    }

    public int getCreditCompleted() {
        return creditCompleted;
    }

    public void setCreditCompleted(int creditCompleted) {
        this.creditCompleted = creditCompleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNumberOfCourses() {
        return numberOfCourses;
    }

    public void addCourse(StudentCourse course) throws Exception {
        if (courses.length == 0)
            throw new Exception("Error : Can not add Course, add CGPA to fix this\n");
        else if (numberOfCourses < courses.length)
            courses[numberOfCourses++] = course;
        else
            throw new Exception("You are not allowed to take more than " + courses.length + " courses\n");
    }

    public void completeSemester() {
        try {
            if (numberOfCourses < 1) {
                System.out.println("No course were taken\n");
                return;
            }

            double sum = 0;
            int newCredit = 0;
            for (int i = 0; i < numberOfCourses; i++) {
                sum += courses[i].getGrade() * courses[i].getCredit();
                newCredit += courses[i].getCredit();
            }

            sum += cgpa * creditCompleted;
            creditCompleted += newCredit;
            cgpa = sum / creditCompleted;

            courses = null;
            numberOfCourses = 0;
            setCoursesLimit(cgpa);

            System.out.println("Congratulations, semester completed\n");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void printCourses() {
        if (numberOfCourses > 0) {
            System.out.println("Course details :");
            for (int i = 0; i < numberOfCourses; i++)
                System.out.println(courses[i].toString());
        } else
            System.out.println("No courses has been taken yet\n");
    }

    @Override
    public String toString() {
        return super.toString()
                + "CGPA\t: " + getCgpa()
                + "\nId\t: " + getId()
                + '\n';
    }
}
