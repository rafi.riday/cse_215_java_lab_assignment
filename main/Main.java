package main;

import main.course.FacultyCourse;
import main.course.StudentCourse;
import main.person.Student;
import main.person.faculty.PermanentFaculty;
import main.person.faculty.VisitingFaculty;

public class Main {
    public static void main(String[] args) {
        System.out.println("--------------------------------------------------");
        // 1
        Student s1 = new Student("Dave Mustaine", 23, "mirpur, dhaka 1216", 3.1, "16112839234", 22);

        try {
            s1.addCourse(new StudentCourse("Programming Language 2", "215", 3));
            s1.addCourse(new StudentCourse("Programming Language 1", "115", 3));
            s1.addCourse(new StudentCourse("Data Structures and Algorithms", "225", 3));
            s1.addCourse(new StudentCourse("Microprocessors", "331", 3));
            s1.addCourse(new StudentCourse("Database Management", "311", 3));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println(s1.toString());
        s1.printCourses();
        System.out.println("--------------------------------------------------");

        // 2
        try {
            s1.addCourse(new StudentCourse("Operating Systems", "323", 3));
            s1.addCourse(new StudentCourse("DLD", "231", 3));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println(s1.toString());
        s1.printCourses();
        System.out.println("--------------------------------------------------");

        // 3
        Student s2 = new Student("Dimebag", 24, "Dhanmondi, Dhaka 1207", 2.4, "1712184642", 15);

        try {
            s2.addCourse(new StudentCourse("Programming Language 2", "215", 3));
            s2.addCourse(new StudentCourse("Programming Language 1", "115", 3));
            s2.addCourse(new StudentCourse("Data Structures and Algorithms", "225", 3));
            s2.addCourse(new StudentCourse("Microprocessors", "331", 3));
            s2.addCourse(new StudentCourse("Database Management", "311", 3));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println(s2.toString());
        s2.printCourses();
        System.out.println("--------------------------------------------------");

        // 4
        PermanentFaculty f1 = new PermanentFaculty("Slash", 30, "Mirpur");

        try {
            f1.addCourse(new FacultyCourse("Programming Language 2", "215", 3));
            f1.addCourse(new FacultyCourse("Programming Language 1", "115", 3));
            f1.addCourse(new FacultyCourse("Data Structures and Algorithms", "225", 3));
            f1.addCourse(new FacultyCourse("Microprocessors", "331", 3));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println(f1.toString());
        f1.printCourses();
        System.out.println("--------------------------------------------------");

        // 5
        try {
            f1.addCourse(new FacultyCourse("Database Management", "311", 3));
            f1.addCourse(new FacultyCourse("Operating Systems", "323", 3));
            f1.addCourse(new FacultyCourse("DLD", "231", 3));
            f1.addCourse(new FacultyCourse("EEE", "141", 3));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println(f1.toString());
        f1.printCourses();
        System.out.println("--------------------------------------------------");

        // 6
        VisitingFaculty f2 = new VisitingFaculty("ozzy", 28, "RA");

        try {
            f2.addCourse(new FacultyCourse("Microprocessors", "331", 3));
            f2.addCourse(new FacultyCourse("Database Management", "311", 3));
            f2.addCourse(new FacultyCourse("Programming Language 2", "215", 3));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println(f2.toString());
        f2.printCourses();
        System.out.println("--------------------------------------------------");

        // grading system included
        /*
         * Student and teacher has different type of Course,
         * StudentCourse and FacultyCourse
         * 
         * StudentCourse will contain information of grade and visibility of grade
         * FacultyCourse will contain Array of Student objects, and those students will
         * contain StudentCourse objects
         * 
         * When a student is added to a teachers FacultyCourse array,
         * the course will be added to the students course list if it was missing
         */

        // add s1 and s2 as student of faculty f1
        try {
            f1.getCourse("215").addStudent(s1);
            f1.getCourse("215").addStudent(s2);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // see if faculty f1 has 2 students in course 215
        f1.printCourses();
        System.out.println("--------------------------------------------------");

        // grade students by their id
        try {
            f1.getCourse("215").gradeStudent("16112839234", 2.00);
            f1.getCourse("215").gradeStudent("1712184642", 4.00);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // check grade without publishing them
        System.out.println(s1.toString());
        s1.printCourses();
        System.out.println(s2.toString());
        s2.printCourses();

        s1.completeSemester();
        s2.completeSemester();
        System.out.println("--------------------------------------------------");

        // publish grade and check from the student side
        try {
            f1.getCourse("215").publishGradeAll();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println(s1.toString());
        s1.printCourses();
        System.out.println(s2.toString());
        s2.printCourses();
        System.out.println("--------------------------------------------------");

        // manually set grade & publish them for s1 to see if completing semester works
        // after completing semester, new cgpa will be calculated
        // and courses will be empty
        // the limit of courses will be also set based on the new cgpa
        for (StudentCourse c : s1.getCourses()) {
            c.setGrade(3.0); // set variables manually to test the process
            c.setGradePublished(true);
        }
        // s1 before completing semester
        System.out.println(s1.toString());
        s1.printCourses();

        s1.completeSemester();
        // s1 after completing semester
        System.out.println(s1.toString());
        s1.printCourses();
        System.out.println("--------------------------------------------------");
    }
}
